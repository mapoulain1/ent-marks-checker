describe("Check Marks", () => {
    it("Check Marks", () => {
        cy.login();
        cy.visit("https://ent.uca.fr/scolarite/stylesheets/etu/notes.faces");
        cy.get("body").find("a").contains("Notes et résultats").click();
        cy.get("body").find("a").contains(Cypress.env("yearCode")).click();


        let marks = [];
        cy.get("tbody").find("tr").each(x => { 
            cy.wrap(x).find("td").eq(2).invoke("text").then(lesson => {
                cy.wrap(x).find("td").eq(3).invoke("text").then(mark => {

                    const trimmedLesson = lesson.trim().replace("&nbsp", "");
                    const trimmedMark = mark.trim().replace("&nbsp", "");

                    if(trimmedMark.length > 0) {
                        marks.push({lesson: trimmedLesson, mark: trimmedMark});
                    }
                });
            }); 
        }).then(() => {
            cy.writeFile(`../Bot/volume/marks/${Cypress.env("discord")}.json`, marks);
        }); 
    });
});