/// <reference types="cypress" />

declare global {
    namespace Cypress {
        interface Chainable {
            login(): any;
        }
    }
}

Cypress.Commands.add("login", () => {
    cy.clearAllSessionStorage();
    cy.clearAllLocalStorage();
    cy.clearAllCookies();
    cy.visit(Cypress.env("baseUrl"));
    cy.get("#username").type(Cypress.env("username"));
    cy.get("#password").type(Cypress.env("password"));
    return cy.get("button[type=submit]").click();
});