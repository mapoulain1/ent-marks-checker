import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
    },
    specPattern: "**/e2e/*.ts",
    video: false,
    defaultCommandTimeout: 30000,
    env: {
      baseUrl: "https://ent.uca.fr/cas/login?service=https%3A%2F%2Fent.uca.fr%2Fcore%2Fhome",
      username: "",
      password: "",
      discord: "",
      yearCode: "ZY301A/421"
    }
  },
});
