FROM nikolaik/python-nodejs
COPY Cypress/cypress Cypress/cypress
COPY Cypress/cypress.config.ts Cypress/cypress.config.ts
COPY Cypress/package.json Cypress/package.json
COPY Cypress/package-lock.json Cypress/package-lock.json
WORKDIR /Cypress
RUN npm install
RUN apt update
RUN apt install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb

WORKDIR /

COPY Bot/bot.py Bot/bot.py
COPY Bot/.env Bot/.env
COPY Bot/requirements.txt Bot/requirements.txt
RUN mkdir Bot/volume
RUN mkdir Bot/volume/users
RUN mkdir Bot/volume/marks
RUN pip3 install -r Bot/requirements.txt
WORKDIR /Bot
CMD python bot.py
