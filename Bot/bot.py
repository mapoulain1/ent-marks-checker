import os
import discord
import json
from discord.ext import commands
from dotenv import load_dotenv
from jsondiff import diff
from cryptography.fernet import Fernet
import subprocess
import asyncio

load_dotenv(dotenv_path=".env")

token = os.getenv("TOKEN")
key = os.getenv("KEY")

frenet = Fernet(key)

default_intents = discord.Intents.default()
default_intents.members = True
default_intents.message_content = True

bot = commands.Bot(command_prefix="!", intents=default_intents)

def in_channel(channel_name):
    def predicate(ctx):
        try:
            return ctx.channel.name == channel_name
        except:
            return False

    return commands.check(predicate)


@bot.command(name="register")
@in_channel("register")
async def get_info(ctx):
    user_info = ctx.message.content.split(" ")
    if len(user_info) != 3:
        await ctx.channel.send("usage : \n!register [prenom] [nom]")
        return

    prenom = user_info[1]
    nom = user_info[2]
    discord = f"{ctx.author}"

    # save info in new json file
    with open(f"volume/users/{discord}.json", "w") as f:
        json.dump(
            {"surname": prenom, "name": nom, "username": "", "password": ""},
            f,
            indent=4,
        )

    await ctx.message.add_reaction("✅")
    await ctx.author.send(
        content=f"Salut {prenom} !\nPrécise ton nom d'utilisateur et mot de passe ENT avec les commandes suivantes : !username et !password"
    )


@bot.command(name="username")
async def get_username(ctx):
    user_info = ctx.message.content.split(" ")
    if len(user_info) != 2:
        await ctx.channel.send("usage : \n!username [username]")
        return

    discord = f"{ctx.author}"
    username = user_info[1]

    if not os.path.exists(f"volume/users/{discord}.json"):
        await ctx.channel.send("Vous n'êtes pas enregistré !")
        return

    with open(f"volume/users/{discord}.json", "r") as f:
        data = json.load(f)
        data["username"] = username

    with open(f"volume/users/{discord}.json", "w") as f:
        json.dump(data, f, indent=4)

    await ctx.message.add_reaction("✅")

    if data["username"] != "" and data["password"] != "":
        await send_marks(discord, ctx.author.send)


@bot.command(name="password")
async def get_password(ctx):
    user_info = ctx.message.content.split(" ")
    if len(user_info) != 2:
        await ctx.channel.send("usage : \n!password [password]")
        return

    password = str(frenet.encrypt(bytes(user_info[1], "utf-8")), "utf-8")
    discord = f"{ctx.author}"

    if not os.path.exists(f"volume/users/{discord}.json"):
        await ctx.channel.send("Vous n'êtes pas enregistré !")
        return

    with open(f"volume/users/{discord}.json", "r") as f:
        data = json.load(f)
        data["password"] = password

    with open(f"volume/users/{discord}.json", "w") as f:
        json.dump(data, f, indent=4)

    await ctx.message.add_reaction("✅")
    await ctx.author.send(content="Il est conseillé de supprimer ce message !")

    if data["username"] != "" and data["password"] != "":
        await send_marks(discord, ctx.author.send)

async def send_marks(author, send):
    discord = f"{author}"

    if not os.path.exists(f"volume/users/{discord}.json"):
        await send("Vous n'êtes pas enregistré !")
        return

    with open(f"volume/users/{discord}.json", "r") as f:
        data = json.load(f)
        username = data["username"]
        password = data["password"]

        password = str(frenet.decrypt(bytes(password, "utf-8")), "utf-8")

        if not os.path.exists(f"volume/marks/{discord}.json"):
            marks = []
        else:
            with open(f"volume/marks/{discord}.json", "r") as f:
                marks = json.load(f)

        output = subprocess.call(f'npm --prefix ../Cypress/ run run -- --env username={username},password="{password}",discord={discord}',shell=True)

        if output == 0:
            with open(f"volume/marks/{discord}.json", "r", encoding = "utf-8") as f:
                data = json.load(f)
                
                if len(data) > len(marks):
                    number = len(data) - len(marks)
                    await send(f"{number} nouvelle{'s' if number > 1 else ''} note{'s' if number > 1 else ''} !")
                    for element in data:
                        if element not in marks:
                            await send(f"{element['lesson']} : {element['mark']}")
                
@bot.event
async def on_ready():
    print(f"Logged in as {bot.user.name}")

    while True:
        users = os.listdir("volume/users")
        for user in users:
            data = user.split(".")[0].split("#")
            discord_user = discord.utils.get(bot.get_all_members(), name=data[0], discriminator=data[1])
            if discord_user is not None:
                await send_marks(f"{data[0]}#{data[1]}", discord_user.send)

        await asyncio.sleep(60 * 60)

bot.run(token)

